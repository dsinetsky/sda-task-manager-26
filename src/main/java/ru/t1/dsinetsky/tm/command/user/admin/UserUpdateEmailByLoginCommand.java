package ru.t1.dsinetsky.tm.command.user.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserUpdateEmailByLoginCommand extends AbstractAdminCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_UPDATE_USER_EMAIL_BY_LOGIN;

    @NotNull
    public static final String DESCRIPTION = "Updates email of user (if any) found by login";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login of user:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter new email:");
        @Nullable final String email = TerminalUtil.nextLine();
        getUserService().updateEmailByLogin(login, email);
        System.out.println("User email successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
