package ru.t1.dsinetsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    void add(AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@NotNull String commandName);

    @Nullable
    AbstractCommand getCommandByArgument(@NotNull String argument);

}
